﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AuthFace.Modelos
{
    public class Emocion
    {
        public string Nombre { get; set; }
        public string Foto { get; set; }
        public float Score { get; set; }
    }
}
