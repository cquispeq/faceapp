﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AuthFace.Helpers
{
    public static class Constantes
    {
        public static readonly string FaceApiURL = "https://authface.cognitiveservices.azure.com/face/v1.0";
        public static readonly string FaceApiKey = "68fb4dd6ac044f398236d41568b7edf5";

        public static readonly string FaceGroupID = "sistemas";
        public static readonly string FaceGroupName = "deptosistemas";
        public static readonly string FaceGroupDescription = "Repositorio para autenticacion por reconocimiento facial";

        public static readonly string NombreBD = "AuthFaceBD.db";
    }
}
