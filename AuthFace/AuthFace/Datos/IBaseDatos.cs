﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AuthFace.Datos
{
    public interface IBaseDatos
    {
        string GetDatabasePath();
    }
}
