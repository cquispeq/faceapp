﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Threading.Tasks;
using AuthFace.Modelos;

namespace AuthFace.Servicios
{
    public interface IServicioBaseDatos
    {
        Task<Usuario> ObtenerUsuario(string key);
        Task<bool> RegistrarUsuario(Usuario dato);
        Task<bool> ActualizarUsuario(Usuario dato);
    }
}
