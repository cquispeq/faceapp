﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AuthFace.Datos;
using AuthFace.Helpers;
using AuthFace.Modelos;

namespace AuthFace.Servicios
{
    public class ServicioBaseDatos : IServicioBaseDatos
    {
        private readonly BaseDatos bd;

        public ServicioBaseDatos()
        {
            bd = new BaseDatos(Constantes.NombreBD);
        }

        public async Task<Usuario> ObtenerUsuario(string key)
        {
            try
            {
                return await bd.Usuarios.FirstOrDefaultAsync(x => x.Key == key);
            }
            catch (Exception ex)
            {
                var textMessage = ex.Message;
                return null;
            }
        }

        public async Task<bool> RegistrarUsuario(Usuario dato)
        {
            try
            {
                await bd.Usuarios.AddAsync(dato);
                await bd.SaveChangesAsync();
                return true;
            }
            catch(Exception ex)
            {
                var textMessage = ex.Message;
                return false;
            }
        }

        public async Task<bool> ActualizarUsuario(Usuario dato)
        {
            try
            {
                bd.Usuarios.Update(dato);
                await bd.SaveChangesAsync();
                return true;
            }
            catch(Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
        }
    }
}
