﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Threading.Tasks;
using Plugin.Media;
using Plugin.Media.Abstractions;

namespace AuthFace.Servicios
{
    public static class ServicioImagen
    {
        public static async Task<MediaFile> TomarFoto()
        {
            MediaFile foto = null;

            try
            {
                //await CrossMedia.Current.Initialize();
                //var photo = CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions() { });

                
                await CrossMedia.Current.Initialize();

                if(CrossMedia.Current.IsCameraAvailable || CrossMedia.Current.IsTakePhotoSupported)
                {
                    foto = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                    {
                        SaveToAlbum = true,
                        Name = "emotion.jpg"
                    }); ;
                }
            }
            catch (Exception ex)
            {
                var textMessage = ex.Message;
                
            }

            return foto;
        }
    }
}
