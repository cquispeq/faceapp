﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using AuthFace.Servicios;

using System.IO;
using Plugin.Media.Abstractions;
using Plugin.Permissions.Abstractions;
using Microsoft.ProjectOxford.Face;
using Microsoft.ProjectOxford.Face.Contract;
using AuthFace.Helpers;
using AuthFace.Modelos;

namespace AuthFace.Paginas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaginaLogin : ContentPage
    {
        public PaginaLogin()
        {
            InitializeComponent();
        }

        private void BtnLogin_Clicked(object sender, EventArgs e)
        {

        }

        void Loading(bool mostrar)
        {
            indicator.IsEnabled = mostrar;
            indicator.IsRunning = mostrar;
        }

        async void btnLogin_Clicked(object sender, EventArgs e)
        {
            try
            {
                Loading(true);
                var foto = await ServicioImagen.TomarFoto();

                if(foto != null)
                {
                    imagen.Source = ImageSource.FromStream(foto.GetStream);

                    var faceId = await ServicioFace.DetectarRostro(foto.GetStream());
                    var personId = await ServicioFace.IdentificarEmpleado(faceId);

                    if(personId != Guid.Empty)
                    {
                        var bd = new ServicioBaseDatos();
                        var usuario = await bd.ObtenerUsuario(personId.ToString());
                        usuario.FotoActual = foto.Path;

                        var emocion = await ServicioFace.ObtenerEmocion(foto);
                        usuario.EmocionActual = emocion.Nombre;
                        usuario.ScoreActual = emocion.Score;
                        var update = await bd.ActualizarUsuario(usuario);

                        await DisplayAlert("Correcto", $"Bienvenido {usuario.Nombre}", "OK");
                        await Navigation.PushAsync(new PaginaBienvenida(usuario));
                    }
                    else
                    {
                        await DisplayAlert("Error", "Persona no identificada", "OK");
                    }
                }
                else
                {
                    await DisplayAlert("Error", "No se pudo tomar la fotografia", "OK");
                }
            }
            catch(Exception ex)
            {
                string textMessage = ex.Message;
                await DisplayAlert("Error", textMessage, "OK");
            }
            finally
            {
                Loading(false);
            }
        }

        async void btnRegistrar_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PaginaRegistro());
        }

        async void btnGrupo_Clicked(object sender, EventArgs e)
        {
            /*
            try
            {
                var clienteFace = new FaceServiceClient(Constantes.FaceApiKey, Constantes.FaceApiURL);
                await clienteFace.CreatePersonGroupAsync(Constantes.FaceGroupID, Constantes.FaceGroupName, Constantes.FaceGroupDescription);

            }
            catch(Exception ex)
            {
                await DisplayAlert("Error",ex.Message, "OK");
            }
            */
            
            if(await ServicioFace.CrearGrupoEmpleados())
            {
                await DisplayAlert("Correcto", "Grupo creado exitosamente", "OK");
            }
            else
            {
                await DisplayAlert("Error", "Error al crear grupo", "OK");
            }
        }

        async void btnEntrenar_Clicked(object sender, EventArgs e)
        {
            if(await ServicioFace.EntrenarGrupoEmpleados())
            {
                await DisplayAlert("Correcto", "Grupo entrenado exitosamente", "OK");
            }
            else
            {
                await DisplayAlert("Error", "Error al crear grupo", "OK");
            }
        }
    }
}