﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using System.IO;
using Xamarin.Forms;
using AuthFace.Droid.Datos;
using AuthFace.Datos;
using AuthFace.Helpers;

[assembly: Dependency(typeof(BaseDatosAndroid))]

namespace AuthFace.Droid.Datos
{
    class BaseDatosAndroid : IBaseDatos
    {
        public string GetDatabasePath()
        {
            return Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), Constantes.NombreBD);
        }
    }
}